#!/bin/bash

PKG=bubhub_snakemake
CONDAROOT=/restricted/projectnb/mlhd/conda
CONDAPREFIX=`conda info -a | grep sys.prefix | sed 's/sys.prefix: //'`
conda remove ${PKG}
grep -l ${PKG} ${CONDAROOT}/envs/.pkgs/cache/* | xargs -n 1 -I {} rm {}
rm -irf ${CONDAROOT}/envs/.pkgs/${PKG}* \
       ${CONDAROOT}/envs/_build/lib/python3.5/site-packages/${PKG}* \
       ${CONDAROOT}/envs/_test/lib/python3.5/site-packages/${PKG}* \
       ${CONDAROOT}/conda-bld/*/${PKG}* \
       ${CONDAROOT}/conda-bld/src_cache/${PKG}* \
       ${CONDAPREFIX}/lib/python3.5/site-packages/${PKG}*

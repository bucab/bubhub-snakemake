from setuptools import setup, find_packages

from glob import glob
import os
import sys

from setuptools.command.test import test as TestCommand

# from http://doc.pytest.org/en/latest/goodpractices.html
# conda skeleton is having trouble installing pytest-runner as suggested
# above, so putting in the command manually
class PyTest(TestCommand):

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ["-x","tests"]

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)

# need to list the data files explicitly
data_files = []
for dn, sd, fns in os.walk(os.path.join("bubhub_snakemake","snakemake")) :
  dn = dn.replace("bubhub_snakemake/","")
  for fn in fns :
    data_files.append(os.path.join(dn,fn))

setup(
  name="bubhub_snakemake"
  ,version=open("VERSION").read().strip()
  ,scripts=['scripts/bhsm.py']
  ,packages=["bubhub_snakemake"]
  ,package_data={"bubhub_snakemake":data_files}
  ,tests_require=['pytest']
  ,cmdclass = {'test': PyTest}
)

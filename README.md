This repository contains Snakefile workflows, rules, and utilities for performing
analyses conducted by the Hub.

[Documentation](http://bubhub_snakemake.readthedocs.io/en/latest/)

The snakemake workflows used by the Hub are tracked in [bubhub-snakemake-workflows](https://bitbucket.org/bubioinformaticshub/bubhub-snakemake-workflows).
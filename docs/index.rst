.. bubhub_snakemake documentation master file, created by
   sphinx-quickstart on Wed Aug  3 18:48:55 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bubhub_snakemake's documentation!
============================================

.. Contents:
   .. toctree::
      :maxdepth: 2

      intro
      guide
      util

.. toctree::
  :maxdepth: 2

  intro
  guide
  util


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


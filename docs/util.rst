Workflow utility functions
==========================

Functions
---------

.. autofunction:: bubhub_snakemake.util.make_opts_str

.. autofunction:: bubhub_snakemake.util.merge_config

.. autofunction:: bubhub_snakemake.util.check_config

.. autofunction:: bubhub_snakemake.util.merge_check_config

.. autofunction:: bubhub_snakemake.util.opt_gen

.. autofunction:: bubhub_snakemake.util.resource_walk

.. autofunction:: bubhub_snakemake.util.find_file_pattern_path_from_walk

.. autofunction:: bubhub_snakemake.util.find_file_pattern_path

.. autofunction:: bubhub_snakemake.util.find_pkg_pattern_path

.. autofunction:: bubhub_snakemake.util.make_config_rule

Exceptions
----------

.. autoclass:: bubhub_snakemake.util.UnknownParameterError

.. autoclass:: bubhub_snakemake.util.RequiredParameterError



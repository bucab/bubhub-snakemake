import pytest
import os

def test_find_rules(monkeypatch) :
  from bubhub_snakemake.rules import find_rules, rule_paths, RulesNotFoundException
  assert 'bubhub_snakemake/snakemake' in find_rules("rseqc.rules")
  with pytest.raises(RulesNotFoundException) :
    find_rules("notfound.rules")

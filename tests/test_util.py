import os
import pytest

def test_exceptions() :
  from bubhub_snakemake.util import RequiredParameterError, UnknownParameterError
  assert str(RequiredParameterError("...")) == "REQUIRED: ..."
  assert str(UnknownParameterError("...")) == "UNKNOWN: ..."

  from bubhub_snakemake.util import required, unknown
  assert str(required("...")) == "REQUIRED: ..."
  assert str(unknown("...")) == "UNKNOWN: ..."

  from bubhub_snakemake.util import ParameterError
  assert isinstance(required(),ParameterError)

def test_make_opts_str() :
  from bubhub_snakemake.util import make_opts_str
  opts = {
     "-a":0
    ,"--b":"a"
    ,"-c": False
    ,"-d": True
    ,"-e": None
    ,"-f": 1
  }
  opts_str = make_opts_str(opts,sep=" ")
  assert "-a 0" in opts_str
  assert "--b a" in opts_str
  assert "-c" not in opts_str
  assert "-d" in opts_str
  assert "-f 1" in opts_str

  opts_str = make_opts_str(opts,sep="=")
  assert "--b=a" in opts_str

def test_merge_config() :
  from bubhub_snakemake.util import check_config, merge_config, unknown
  defaults = {
    "a": 1
    ,"b": "2"
    ,"c": {
      "c1" : 1
      ,"c2" : 2
    }
    ,"d": None
  }

  user = {
    "a": 1
    ,"b": "3"
    ,"c": {
      "c2" : 3
    }
    ,"e": "new"
  }

  config = merge_config(user,defaults)
  assert config["a"] == 1 # in user config, unchanged
  assert config["b"] == "3" # in user config, changed
  assert config["c"]["c1"] == 1 # not in user config
  assert config["c"]["c2"] == 3 # in user config, changed
  assert config["d"] is None # missing from user config
  assert "e" in config # not in defaults
  assert isinstance(config["e"],unknown) # strict sets unknown parameters to UnknownParameterError

  user = {
    "e": "new"
  }
  config = merge_config(user,defaults,strict=True)
  with pytest.raises(unknown) :
    check_config(config)
  config = merge_config(user,defaults,strict=False)
  assert config["e"] == "new"

  user = {
    "e": {"a":"new"}
  }
  config = merge_config(user,defaults,strict=False)
  assert config["e"]["a"] == "new"

# eval_config is a bad idea, but these tests pass so might as well leave them
def test_eval_config() :
  from bubhub_snakemake.util import eval_config, RequiredParameterError
  config = {"a":1}
  assert eval_config(config)["a"] == 1

  config = {"a":"var"}
  assert eval_config(config)["a"] == "var"

  # check missing local import
  config = {"a":"required('...')"}
  config = eval_config(config)
  assert config["a"] == "required('...')"

  from bubhub_snakemake.util import required
  config = eval_config(config)
  assert isinstance(config["a"],RequiredParameterError)

def test_check_config() :
  from bubhub_snakemake.util import check_config, required, unknown
  config = {"a": required("") }
  with pytest.raises(required) :
    check_config(config)

  config = {"a": {"b": required("") } }
  with pytest.raises(required) :
    check_config(config)

  config = {"a": {"b": required("") } }
  assert check_config(config,raise_errors=False) is False

  config = {"a": 0}
  assert check_config(config) is True

def test_merge_check_config() :
  from bubhub_snakemake.util import merge_check_config, unknown
  defaults = {
    "a": 1
    ,"b": "2"
    ,"c": {
      "c1" : 1
      ,"c2" : 2
    }
    ,"d": None
  }

  user = {
    "a": 1
    ,"b": "3"
    ,"c": {
      "c2" : 3
    }
  }

  config = merge_check_config(user,defaults)
  assert config["a"] == 1 # in user config, unchanged
  assert config["b"] == "3" # in user config, changed
  assert config["c"]["c1"] == 1 # not in user config
  assert config["c"]["c2"] == 3 # in user config, changed
  assert config["d"] is None # missing from user config

  user = {
    "e": "new"
  }
  # strict and raise
  with pytest.raises(unknown) :
    config = merge_check_config(user,defaults,strict=True,raise_errors=True)

  # strict and no raise
  config = merge_check_config(user,defaults,strict=True,raise_errors=False)
  assert isinstance(config["e"],unknown)

  # no strict and raise
  config = merge_check_config(user,defaults,strict=False,raise_errors=True)
  assert config["e"] == "new"

  # no strict and no raise
  config = merge_check_config(user,defaults,strict=False,raise_errors=False)
  assert config["e"] == "new"

def test_opt_gen() :
  from bubhub_snakemake.util import opt_gen
  d = {
    "a": 3
    ,"b": {
      "b1": 1
    }
  }
  opts = opt_gen(d)
  assert opts()["a"] == 3
  assert opts("a") == 3
  assert opts("a",default=4) == 3
  assert opts("a",set=5) == 5
  assert opts("b","b1") == 1
  assert opts("b","b1",set=10) == 10
  assert opts("b","b2",default=5) == 5
  assert opts("b","b2") is None
  assert opts("b","b2",set=10) == 10
  assert opts("b","b2") == 10
  assert opts("c") == None
  assert opts("c",default=4) == 4
  with pytest.raises(Exception) :
    opts("a",default=4,set=5)

@pytest.fixture
def test_file_tree() :
  paths = {
    ".": [
      ('.',('dir1',),('file1.txt',))
      ,('./dir1',('subdir1','subdir2'),('dir_file1.txt','dir_file1.err'))
      ,('./dir1/subdir1',(),('subdir1_file1.txt','subdir1_file1.err'))
      ,('./dir1/subdir2',(),())
    ]
    ,"dir1": [
      ('dir1',('subdir1','subdir2'),('dir_file1.txt','dir_file1.err'))
      ,('dir1/subdir1',(),('subdir1_file1.txt','subdir1_file1.err'))
      ,('dir1/subdir2',(),())
    ]
  }
  return paths

def test_find_file_pattern_path(monkeypatch,test_file_tree) :
  from bubhub_snakemake.util import find_file_pattern_path
  def mockwalk(path) :
    paths = test_file_tree[path]
    for p in paths :
      yield p

  monkeypatch.setattr(os, 'walk', mockwalk)

  def mockabspath(path) :
    return "abs/"+path
  monkeypatch.setattr(os.path, 'abspath', mockabspath)

  ffpp = find_file_pattern_path
  # exact filename match
  assert ffpp("file1.txt",".") == ["abs/./file1.txt"]
  assert ffpp("file1.txt",".",abs_path=False)[0] == "./file1.txt"

  # suffix filename match
  assert ffpp(".*.txt",".") == ["abs/./file1.txt"]
  assert len(ffpp(".*.txt",".",all_paths=True)) == 3
  assert ffpp(".*\\.txt",".",all_paths=True) == [
    "abs/./file1.txt"
    ,"abs/./dir1/dir_file1.txt"
    ,"abs/./dir1/subdir1/subdir1_file1.txt"
  ]

  # incomplete match
  assert ffpp("le1.txt",".",complete=True) == []
  assert ffpp("le1.txt",".",complete=False) == ["abs/./file1.txt"]

  # absolute path == False
  assert ffpp("file1.txt",".",abs_path=False)[0] == "./file1.txt"

  # file not found
  assert len(ffpp("blah.txt",".")) == 0

  # same file with different root directories
  assert ffpp("subdir1_file1.err",".")[0] == "abs/./dir1/subdir1/subdir1_file1.err"
  assert ffpp("subdir1_file1.err","dir1")[0] == "abs/dir1/subdir1/subdir1_file1.err"


def mocklistdir(pkg_res,res_name) :
  d = {
    'data': ['dir1','file1.txt']
    ,'data/dir1': ['subdir1','file2.txt']
    ,'data/dir1/subdir1': ['file3.txt','file4.txt']
  }
  return d[res_name]

def mockisdir(pkg_res,res_name) :
  return res_name in ['data','data/dir1','data/dir1/subdir1']

def test_resource_walk(monkeypatch,test_file_tree) :
  from bubhub_snakemake.util import resource_walk
  import pkg_resources

  monkeypatch.setattr(pkg_resources,'resource_listdir',mocklistdir)
  monkeypatch.setattr(pkg_resources,'resource_isdir',mockisdir)

  assert list(resource_walk('x','data'))[0] == ('data',['dir1'],['file1.txt'])
  assert list(resource_walk('x','data'))[1] == ('data/dir1',['subdir1'],['file2.txt'])
  assert list(resource_walk('x','data'))[2] == ('data/dir1/subdir1',[],['file3.txt','file4.txt'])
  assert list(resource_walk('x','data/dir1/subdir1'))[0] == ('data/dir1/subdir1',[],['file3.txt','file4.txt'])

def test_find_pkg_pattern_path(monkeypatch) :
  from bubhub_snakemake.util import find_pkg_pattern_path
  import pkg_resources

  monkeypatch.setattr(pkg_resources,'resource_listdir',mocklistdir)
  monkeypatch.setattr(pkg_resources,'resource_isdir',mockisdir)

  def mockfilename(pkg_name,res_name) :
    return 'abs/./{}'.format(res_name)
  monkeypatch.setattr(pkg_resources,'resource_filename',mockfilename)

  assert find_pkg_pattern_path('file1.txt','x','data') == ['abs/./data/file1.txt']
  assert find_pkg_pattern_path('file4.txt','x','data') == ['abs/./data/dir1/subdir1/file4.txt']

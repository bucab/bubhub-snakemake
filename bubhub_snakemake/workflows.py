import pkg_resources
import os
from bubhub_snakemake.util import find_file_pattern_path, find_pkg_pattern_path

workflow_paths = [os.getcwd()]

def find_workflows(fn) :
  """Locate a workflows file in default locations

  Search default locations for a workflows file named `fn`. Default locations are,
  in order of decreasing precedence:
  
  - current directory, or any subdirectory below it
  - any paths in `bubhub_snakemake.workflows.workflow_paths`
  - all workflows under `bubhub_snakemake.snakemake.*`

  The first file found matching `fn` is returned.  Additional directories may be
  inserted into to `workflow_paths` to control this behavior. Note "." is the first
  entry in this attribute by default, removing this entry prior to calling this
  function suppresses the current directory search behavior.

  :param str fn: search for file with this name, e.g. `test.snake`, if not supplied
      or `None`, return a dictionary of all <workflow fn>:<workflow path> discovered in
      default locations
  :returns: absolute path to the workflows file if found
  :raises :class:`~bubhub_snakemake.workflows.WorkflowNotFoundException`: if the workflows
      path could not be found
  """

  workflow_path = None

  # look in built in directories
  if not workflow_path :
    for path in workflow_paths :
      workflow_path = find_file_pattern_path(fn,path)
      if workflow_path :
        break

  # look in package data
  if not workflow_path :
    workflow_path = find_pkg_pattern_path(fn,'bubhub_snakemake','snakemake')

  # didn't find it
  if not workflow_path :
    raise WorkflowNotFoundException("Workflow file matching {} was not found.".format(fn))

  return workflow_path[0]

class WorkflowNotFoundException(Exception) :
  """Exception raised when a workflows file was requested but not found
  in any of the default paths
  """

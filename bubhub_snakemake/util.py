import collections
import copy
from functools import partial
import json
import os
import pkg_resources
import pprint
import re
import shlex

class ParameterError(Exception) :
  # this is to trick snakemake into allowing these objects to be input
  # as input or output objects, otherwise you get:
  #  Input and output files have to be specified as strings or lists of strings.
  def __call__(self,*args,**kwargs) :
    pass
  pass

class RequiredParameterError(ParameterError) :
  """Exception indicating a required parameter has not been specified."""
  def __str__(self) :
    return "REQUIRED: {}".format(",".join(self.args))
required = RequiredParameterError

class UnknownParameterError(ParameterError) :
  """Exception indicating a parameter is not recognized."""
  def __str__(self) :
    return "UNKNOWN: {}".format(",".join(self.args))
unknown = UnknownParameterError

def make_opts_str(opts,sep=" ") :
  """Translate a dictionary to a string of arguments suitable for the command
  line.

  Translates a dictionary where keys are POSIX-style argument flags (e.g. -a)
  and values are the desired arguments into a string representation. Keys must
  be strings and values must be python builtins, i.e. strings, ints, floats,
  booleans, or None. If a value is None or False, the argument is omitted from
  the final string, otherwise the value is set to the dictionary value.

  Arguments that start with a double hyphen (e.g. --arg) are added to the string
  with their value delimited by the *sep* argument.

  .. note:: Since python dictionaries are unorded, there is no guarantee of the
    order of the options in the resulting string. Passing an instance of
    :py:class:`collections.OrderedDict` will result in an ordered string.

  :param opts: Dictionary-like object
  :param sep: Character string to delimit keys and values starting with double
    hyphens
  :type sep: str

  :returns: string representation of opts dictionary

  Examples::

    >>> make_opts_str({"-a":1,"-b":True,"--c":None,"-d":False})
    "-a 1 -b"
    >>> make_opts_str({"--arg":"five"})
    "--arg five"
    >>> make_opts_str({"--arg":"five"},sep="=")
    "--arg=five"
  """

  opts_str = []
  for o,v in opts.items() :
    if v is True :
      opts_str.append(o)
    elif v is None or v is False :
      continue
    else :
      if o.startswith('--') :
        opts_str.append("{}{}{}".format(o,sep,v))
      else :
        opts_str.append("{} {}".format(o,v))

  opts_str = " ".join(opts_str)
  return opts_str

# this is actually a really bad idea, but I'm going to leave the code
# here anyway in case it seems like a better idea later
# consider controlling the list of things that are eval()able
def eval_config(val) :
  '''Recursively eval() the value(s) of object as python expressions.

  Return a copy of *val* where all strings that are valid python
  expressions are eval()ed into python objects. The structure and
  numeric values are left untouched.

  :param val: any valid python object
  :returns: new copy of *val* with strings replaced with python
    objects where possible
  '''
  # http://stackoverflow.com/questions/6618795/get-locals-from-calling-namespace-in-python
  import inspect
  frame = inspect.currentframe()
  return _eval_config_vars(val
    ,frame.f_back.f_globals
    ,frame.f_back.f_locals
  )

def _eval_config_vars(val,globals,locals) :
  '''eval() needs the caller's local vars to resolve correctly'''
  new_val = copy.deepcopy(val)
  if isinstance(new_val,collections.Mapping) :
    for k,v in new_val.items() :
      new_val[k] = _eval_config_vars(v, globals, locals)
  elif (isinstance(new_val,collections.Iterable) and
        not isinstance(new_val,str) ) :
    new_val = [_eval_config_vars(v, globals, locals) for v in new_val] 
  else :
    # eval runs in the context of the globals in this module, not the
    # calling context. I'm not sure if that's a problem
    try : return eval(new_val,globals,locals)
    except TypeError as e :
      pass
    except NameError as e :
      pass # maybe warn user?
  return new_val


def merge_config(user,defaults,strict=True) :
  """Merge values in dictionary ``user`` into dictionary.

  For each key/value in ``defaults``, set ``user[k] = defaults[k]``
  if ``k not in user``, else leave value in ``user``. If value is
  an instance of :py:class:`collections.Mapping`, recurse into it to
  produce nested dictionaries.

  :param user: dictionary to override defaults
  :type user: dict
  :param defaults: dictionary of default values
  :type defaults: dict
  :param strict: set value of keys in user but not in
    default to an :class:`UnknownParameterError`, so :func:`check_config`
    will raise, otherwise set the default value for these
    keys to the user value
  :type strict: bool
  :returns: dict with values set according to default where
    not set in user

  Examples::

    >>> merge_config({'a':3},{'a':1,'b':9})
    {'a':3,'b':9}
    >>> merge_config({'a':3},{'b':9})
    {'a':UnknownParameterError("..."),'b':9}
    >>> merge_config({'a':3},{'b':9},strict=False)
    {'a':3,'b':9}
  """

  merged = copy.deepcopy(defaults)
  for k,v in user.items() :
    if k not in merged and strict :
      merged[k] = unknown("Parameter was not recognized as an option for this tool (\"{}\"=\"{}\" not in {})".format(k,v,merged))
    elif isinstance(v, collections.Mapping) :
      r = merge_config(v,merged.get(k,{}),strict=strict)
      merged[k] = merged.get(k,{})
      merged[k].update(r)
    else :
      merged[k] = user.get(k,v)
  return merged

def check_config(config,raise_errors=True) :
  """Check for missing required or unknown values in config

  Examine values in dictionary-like ``config`` recursively, looking
  for instances of :class:`UnknownParamterException` or 
  :class:`RequiredParamterException`. If ``raise_errors`` is True,
  raise the exception, otherwise just print it. Returns True

  :param config: dictionary-like object
  :type config: dict
  :param raise_errors: Raise the first exception found, otherwise
    print out the arguments used when instantiating the exception
  :type raise_errors: bool

  :returns: True if no exceptions were raised, or False if exceptions
    were found and ``raise_errors == False``
  """

  success = True
  for k,v in config.items() :
    if isinstance(v, collections.Mapping) :
      success = check_config(v,raise_errors=raise_errors)
    elif isinstance(v,Exception) :
      success = False
      if raise_errors :
        raise v
      else :
        print(str(v))
  return success

def merge_check_config(
  user
  ,defaults
  ,strict=True
  ,raise_errors=True
  ) :
  """Combines :func:`merge_config` and :func:`check_config`.

  Arguments are passed appropriately to the two functions called.
  """

  merged = merge_config(user,defaults,strict=strict)
  check_config(merged,raise_errors=raise_errors)
  return merged

def merge_eval_check_config(
  user
  ,defaults
  ,strict=True
  ,raise_errors=True
  ) :
  """Combines :func:`merge_config`, :func:`eval_config`, and
    :func:`check_config`.

  Arguments are passed appropriately to the three functions called.
  """

  merged = merge_config(user,defaults,strict=strict)
  merged = eval_config(merged)
  check_config(merged,raise_errors=raise_errors)
  return merged

def opt_gen(d) :
  """Generator for accessing nested dictionaries and providing default values.

  Create a closure for dictionary ``d`` to make accessing arbitrary
  sub-dictionaries convenient. Returns a function with the following signature::

    f([key,[subkey,[subsubkey,[...]]]],default=None,set=None)

  Example::

    >>> opts = opt_gen({'a':1,'b':{'c':3}})
    >>> opts("a")
    1
    >>> opts("b","c")
    3
    >>> opts("b","d")
    None

  The argument ``default`` specifies the value to be returned if the key does
  not exist but leaves the original dictionary intact, while the ``set``
  argument returns the set value first and then returns it ::
    
    >>> d = {"a":1}
    >>> opts = opt_gen(d)
    >>> opts("b")
    None
    >>> opts("b",default=5)
    5
    >>> opts("b")
    None
    >>> opts("b",set=10)
    10
    >>> opts("b")
    10

  :param d: dictionary-like object
  :type d: dict

  :returns: function closure with ``d`` accessible as described above
  """
  def f(*keys,default=None,set=None) :
    if default and set :
      raise Exception("default and set cannot both be specified")
    curr_d = d
    if len(keys) == 0 :
      return d
    for k in keys[:-1] :
      curr_d = curr_d.get(k,{})
    if set :
      curr_d[keys[-1]] = set
    return curr_d.get(keys[-1],default)
  return f

def resource_walk(pkg_name,resource_name) :
  """:py:func:`os.walk` for package resources

  Walk through the resource `pkgname` and return output as :py:func:`os.walk`
  would, i.e. a generator with tuples of ("dirname",[subdirs],[filenames]). The
  arguments are the same as to the
  [Basic Resource Access](http://setuptools.readthedocs.io/en/latest/pkg_resources.html#basic-resource-access)
  functions.

  :param str pkg_name: package or requirement name
  :param str resource_name: resource directory name
  :returns: a generator that walks the package resource tree like :py:func:`os.walk`
  """
  pkg_res = pkg_resources.resource_listdir(pkg_name,resource_name)
  dirnames = [_ for _ in pkg_res if pkg_resources.resource_isdir(
    pkg_name
    ,os.path.join(resource_name,_)
  )]
  filenames = [_ for _ in pkg_res if _ not in dirnames]

  # this dir
  yield (resource_name,dirnames,filenames)

  # recurse
  for dirname in dirnames :
    for res in resource_walk(pkg_name,os.path.join(resource_name,dirname)) :
      yield res

def find_file_pattern_path_from_walk(
  patt
  ,walk
  ,all_paths=False
  ,abs_path=True
  ,abs_f=lambda x: x
  ,complete=True
  ) :
  """Find a file matching regular expression in a directory walk sequence

  Scan the iterable `walk` for files matching the regular expression `patt`.
  The `walk` iterable should be like that produced by :py:func:`os.walk`, would,
  i.e. a generator with tuples of ("dirname",[subdirs],[filenames]).  Return the
  first path found, unless `all_paths` is True, in which case return all
  matching paths.

  :param str patt: regular expression pattern
  :param str walk: iterable of values like that of :py:func:`os.walk`
  :param bool all_paths: return all matching paths, instead of first encountered
  :param bool abs_path: return absolute rather than relative path
  :param function abs_f: function accepting one argument that returns the
      absolute path of the file if found
  :param bool complete: wrap the pattern patt as `^patt$`
  :returns: list of paths to files matching pattern, empty if none found
  """
  abs = abs_f if abs_path == True else lambda x: x
  patt = "^{}$".format(patt) if complete else patt
  paths = []
  for dname, dirs, files in walk :
    for fn in files :
      if re.search(patt,fn) :
        if all_paths == False :
          return [abs(os.path.join(dname,fn))]
        else :
          paths.append(abs(os.path.join(dname,fn)))
  return paths

def find_file_pattern_path(
  patt
  ,dirname
  ,all_paths=False
  ,abs_path=True
  ,complete=True
  ) :
  """Find a file matching regular expression in a directory tree

  See :func:`find_file_pattern_path_from_walk` for the meaning of parameters.

  :param str dirname: name of directory to recursively search
  :returns: list of paths to files matching pattern, empty if none found
  """

  return find_file_pattern_path_from_walk(
    patt
    ,os.walk(dirname)
    ,all_paths=all_paths
    ,abs_path=abs_path
    ,abs_f=os.path.abspath
    ,complete=complete
  )

def find_pkg_pattern_path(
  patt
  ,pkg_name
  ,res_name
  ,all_paths=False
  ,abs_path=True
  ,complete=True
  ) :
  """Find a file matching regular expression in a package resource directory 

  See :func:`find_file_pattern_path_from_walk` for the meaning of parameters.
  :param str pkg_name: name of package to search
  :param str res_name: resource name within package to search
  :returns: list of paths to files matching pattern, empty if none found
  """

  return find_file_pattern_path_from_walk(
    patt
    ,resource_walk(pkg_name,res_name)
    ,all_paths=all_paths
    ,abs_path=abs_path
    ,abs_f=partial(pkg_resources.resource_filename,pkg_name)
    ,complete=complete
  )

def json_str_default(o) :
  if isinstance(o,ParameterError) :
    return str(o)
  else :
    return json.JSONEncoder.default(self,o)

def dump_config(config,fn) :
  """Write pretty-printed config to file *fn*.
  
  :param str fn: filename to write config to
  :param dict config: dictionary of configurations to write
  :returns: None
  """
  with open(fn,'w') as f :
    json.dump(
      config
      ,f
      ,sort_keys=True
      ,indent=4
      ,default=json_str_default
    )

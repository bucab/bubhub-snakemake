import pkg_resources
import os
from bubhub_snakemake.util import find_file_pattern_path, find_pkg_pattern_path

rule_paths = [os.getcwd()]

def find_rules(fn) :
  """Locate a rules file in default locations

  Search default locations for a rules file named `fn`. Default locations are,
  in order of decreasing precedence:
  
  - current directory, or any subdirectory below it
  - any paths in `bubhub_snakemake.rules.rule_paths`
  - all rules under `bubhub_snakemake.snakemake.*`

  The first file found matching `fn` is returned.  Additional directories may be
  inserted into to `rule_paths` to control this behavior. Note "." is the first
  entry in this attribute by default, removing this entry prior to calling this
  function suppresses the current directory search behavior.

  :param str fn: search for file with this name, e.g. `test.rules`, if not supplied
      or `None`, return a dictionary of all <rule fn>:<rule path> discovered in
      default locations
  :returns: absolute path to the rules file if found
  :raises :class:`~bubhub_snakemake.rules.RulesNotFoundException`: if the rules
      path could not be found
  """

  rule_path = None

  # look in built in directories
  if not rule_path :
    for path in rule_paths :
      rule_path = find_file_pattern_path(fn,path)
      if rule_path :
        break

  # look in package data
  if not rule_path :
    rule_path = find_pkg_pattern_path(fn,'bubhub_snakemake','snakemake')

  # didn't find it
  if not rule_path :
    raise RulesNotFoundException("Rules file matching {} was not found.".format(fn))

  return rule_path[0]

class RulesNotFoundException(Exception) :
  """Exception raised when a rules file was requested but not found
  in any of the default paths
  """

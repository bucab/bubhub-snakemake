"""
Interface into bubhub_snakemake

Usage:
  bhsm.py [options] run <snakefile> [<snakemake_opts>...]
  bhsm.py [options] extract [<rule_or_workflow_file>...]
  bhsm.py [options] info

Options:
  -h --help

  all subcommands:
  -p --search-path=<path>    Include <path> in paths to search for workflows and
                             rules, multiple paths may be specified separated by
                             commas
  -w --workflow-path=<path>  Include <path> in paths to search for workflows,
                             multiple paths may be specified separated by commas
  -r --rule-path=<path>      Include <path> in paths to search for rules,
                             multiple paths may be specified separated by commas

  run options:
  --cmd-out=<cmd_out>      Write out the snakemake command to file, for debugging

  extract options:
  -o --outdir=<outdir>     Directory to extract files to in extract mode
                           [default: .]
  -v --view                Print the files to screen, rather than writing to file

  info options:
  --no-files               Only show paths for the files included in the
                           bubhub_snakemake distro, and not under the current
                           directory (as would be found by find_rules()and
                           find_workflows())
  --no-pkg                 Do not include bubhub_snakemake rules and workflows
  --all-paths              Show all paths found in the workflow and rules search,
                           not just the ones that will be used by workflows
"""


import os
import shutil
import subprocess
import sys
import warnings

from clint.textui import puts, indent, colored

from docopt import docopt

import snakemake

import bubhub_snakemake
from bubhub_snakemake.rules import find_rules, rule_paths, RulesNotFoundException
from bubhub_snakemake.util import find_pkg_pattern_path, find_file_pattern_path
from bubhub_snakemake.workflows import find_workflows, workflow_paths, WorkflowNotFoundException

def get_snakemake_path() :
  return subprocess.check_output(
    ["/usr/bin/which","snakemake"]
  ).strip().decode("utf-8")

def get_snakemake_version() :
  return subprocess.check_output(
    [get_snakemake_path(),"--version"]
  ).strip().decode("utf-8")


if __name__ == "__main__" :

  args = docopt(__doc__,options_first=True)

  if args["--search-path"] :
    paths = [_ for _ in args["--search-path"].split(',') if len(_) != 0]
    workflow_paths.extend(paths)
    rule_paths.extend(paths)

  if args["--workflow-path"] :
    paths = [_ for _ in args["--workflow-path"].split(',') if len(_) != 0]
    workflow_paths.extend(paths)

  if args["--rule-path"] :
    paths = [_ for _ in args["--rule-path"].split(',') if len(_) != 0]
    rule_paths.extend(paths)

  if args["run"] :

    snakefile = args["<snakefile>"]

    # look for a real path
    if os.path.exists(snakefile) :
      workflow = os.path.abspath(snakefile)
    else :
      workflow = find_workflows(snakefile)

    # trick snakemake into thinking it was invoked on the command line
    old_sys_argv = sys.argv
    sys.argv = [get_snakemake_path(),"-s",workflow.strip()] + args["<snakemake_opts>"]

    if args["--cmd-out"] :
      with open(args["--cmd-out"],'w') as f :
        f.write("{} \\\n".format(sys.argv[0]))
        f.write("\\\n".join("    {} ".format(_) for _ in sys.argv[1:]))

    snakemake.main()

    sys.argv = old_sys_argv

  elif args["extract"] :
    for fn in args["<rule_or_workflow_file>"] :

      paths = find_pkg_pattern_path(fn,"bubhub_snakemake","snakemake")

      if not paths :
        warnings.warn("No package file named {} was found.".format(fn))
      else :
        for p in paths :
          if args["--view"] :
            with open(p) as f :
              sys.stdout.write(f.read())
          else :
            _,fn = os.path.split(p)
            shutil.copyfile(
              p
              ,os.path.join(args["--outdir"],fn)
              ,follow_symlinks=True
            )
  elif args["info"] :

    puts(colored.yellow("bubhub_snakemake version: ")+bubhub_snakemake.__version__)
    puts(colored.yellow("snakemake path: ")+get_snakemake_path())
    puts(colored.yellow("snakemake version: ")+get_snakemake_version())
    puts(colored.yellow("workflow_paths: "))
    with indent(2) :
      for path in workflow_paths :
        puts(colored.green("- {}".format(path)))
    puts(colored.yellow("rule_paths: "))
    with indent(2) :
      for path in rule_paths :
        puts(colored.green("- {}".format(path)))

    def printout(items,title) :
      puts(colored.yellow(title+":"))
      with indent(2) :
        for item in items :
          puts(colored.green("- {}".format(item)))

    def get_paths(patt,search_paths) :
      items = []
      if not args["--no-pkg"] :
        items.extend(find_pkg_pattern_path(patt,"bubhub_snakemake","snakemake",all_paths=True))
      if not args["--no-files"] :
        file_items = []
        for p in search_paths :
          file_items.extend(find_file_pattern_path(patt,p,all_paths=True))
        items = sorted(file_items+items,key=lambda x:os.path.split(x)[1])
        item_fns = [os.path.split(_)[1] for _ in items]
        for wf in set(item_fns) :
          wf_i = item_fns.index(wf)
          items[wf_i] += " (*)"
        if not args["--all-paths"] :
          items = [_ for _ in items if _.endswith("(*)")]
      return items

    workflows = get_paths(".*.snake",workflow_paths)
    printout(workflows,"workflows")

    rules = get_paths(".*.rules",rule_paths)
    printout(rules,"rules")

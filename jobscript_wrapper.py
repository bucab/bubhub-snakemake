#!/usr/bin/env python3
import os
import sys

from snakemake.utils import read_job_properties


jobscript = sys.argv[1]
job_properties = read_job_properties(jobscript)

# do something useful with the threads
threads = job_properties.get("threads",1)

# figure out the correct pe to use based on the number of threads
num_tasks = (1,2,4,8,12,16,20)
ceil_tasks = [_ for _ in num_tasks if _-threads>=0]
if not ceil_tasks :
  ceil_tasks = max(num_tasks)
else :
  ceil_tasks = ceil_tasks[0]
pe = ""
if ceil_tasks > 1 :
  pe = '-pe mpi_{}_tasks_per_node {}'.format(ceil_tasks,threads)

cmd = "qsub -P {project} -cwd {pe} {script}".format(
  project=job_properties["cluster"].get("project","mlhd")
  ,pe=pe
  ,script=jobscript
  )
os.system(cmd)

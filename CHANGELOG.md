# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
  - this changelog
  - changed to full semantic versioning
  - --no-pkg option to bhsm.py info

### Changed
  - --pkg-only changed to --no-files in bhsm.py
  - <snakefile> command line argument is now searched for explicitly
    as a file and only searched using find_workflows() if not found
